class Parent{

  public Parent(){
       System.out.println("Parent class");
   }

}
class Son extends Parent{
   public Son(){
        System.out.println("Child class");
    }

}
class DEW{
   public static void main(String[] args) {
       Son obj=new Son();/*I am calling son  class constructor but both constructor print-- why
    because  A parent class contsructor  is not inherited  in child class and that is why 
    super() is addeda automatically in child class contsuctor  if there is no explicit call to super
     or this.   */
   }

}